//
//  ViewController.h
//  se13m007
//
//  Created by System Administrator on 03.01.15.
//  Copyright (c) 2015 Holzer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReposViewController.h"
#import "AppDelegate.h"

@interface ViewController : UIViewController
{
    
    __weak IBOutlet UITextField *usernameTextField;
    __weak IBOutlet UILabel *usernameOutputLabel;
    __weak IBOutlet UIButton *showReposButton;
    
}

- (IBAction)authorize:(id)sender;
- (IBAction)showRepos:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userProfileNameLabel;
@property (nonatomic) NSString *userLoginName;

- (void)loadUserProfile;

@end

