//
//  main.m
//  se13m007
//
//  Created by System Administrator on 03.01.15.
//  Copyright (c) 2015 Holzer. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
