//
//  ReposViewController.h
//  se13m007
//
//  Created by System Administrator on 03.01.15.
//  Copyright (c) 2015 Holzer. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReposViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    
}

@property(nonatomic) NSArray *repos;

@end