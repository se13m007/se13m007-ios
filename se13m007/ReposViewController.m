//
//  ReposViewController.m
//  se13m007
//
//  Created by System Administrator on 03.01.15.
//  Copyright (c) 2015 Holzer. All rights reserved.
//

#import "ReposViewController.h"

@interface ReposViewController ()

@end

@implementation ReposViewController
{
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.repos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if(cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.textLabel.text = [self.repos objectAtIndex:indexPath.row];
    return cell;
}

@end
