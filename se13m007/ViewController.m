//
//  ViewController.m
//  se13m007
//
//  Created by System Administrator on 03.01.15.
//  Copyright (c) 2015 Holzer. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import <AFNetworking/UIImageView+AFNetworking.h>

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    [delegate addObserver:self forKeyPath:@"accessToken" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)loadUserProfile {
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"https://api.github.com/user"
      parameters:@{@"access_token": delegate.accessToken}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSLog(@"JSON: %@", responseObject);
             
             // set name of user, email and login name
             self.userLoginName = [responseObject valueForKey:@"login"];
             self.userProfileNameLabel.text = [responseObject valueForKey:@"name"];
             // load the user avatar image
             NSString *avatarURLString = [responseObject valueForKey:@"avatar_url"];
             NSURL *avatarURL = [NSURL URLWithString:avatarURLString];
             if(avatarURL) {
                 [self.userProfileImageView setImageWithURL:avatarURL];
             }
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

- (IBAction)showRepos:(id)sender {
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *url = [NSString stringWithFormat:@"%@%@%@", @"https://api.github.com/users/", self.userLoginName, @"/repos"];
    [manager GET:url
      parameters:@{@"access_token": delegate.accessToken}
         success:^(AFHTTPRequestOperation *operation, id responseObject) {
             
             NSLog(@"JSON: %@", responseObject);
             
             // create new view with table and show the repos
             UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
             ReposViewController *controller = [storyboard instantiateViewControllerWithIdentifier:@"reposViewController"];
             controller.repos = [responseObject valueForKey:@"name"];
             
             [self presentViewController:controller animated:YES completion:nil];
             
         } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
             NSLog(@"Error: %@", error);
         }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if([keyPath isEqualToString:@"accessToken"]) {
        [self loadUserProfile];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)authorize:(id)sender {
    NSString *authorizeURLString = [NSString stringWithFormat:@"https://github.com/login/oauth/authorize?scope=user:email&client_id=%@", kCLIENT_ID];
    NSURL *authorizeURL = [NSURL URLWithString:authorizeURLString];
    [[UIApplication sharedApplication] openURL:authorizeURL];
    
    showReposButton.enabled = YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

}

@end
