# Übung
* Markus Holzer
* se13m007
* se13m007@technikum-wien.at

# iOS-Projekt:
* Authorisierung mit Github Login (OAuth)
* Anzeigen des Benutzernamens und Profilbild des autorisierten Users
* Beim Klick auf den Button "Repos anzeigen" werden die Repositories des autorisierten Users in einem eigenen TableView angezeigt.
